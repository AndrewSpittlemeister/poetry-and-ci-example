from sample_package import __version__, echo, ohce


def test_version():
    assert __version__ == '0.2.0'

def test_echo():
    assert echo('asdf') is None
