from sample_package import echo


def main() -> None:
    echo("in sample_package.sub_module.main")


if __name__ == '__main__':
    main()
