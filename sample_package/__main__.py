from sample_package import echo


def main() -> None:
    echo("in sample_package.main")


if __name__ == '__main__':
    main()
