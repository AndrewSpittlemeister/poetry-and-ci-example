'''
sample_package
'''

__version__ = '0.2.0'

from sample_package.functions import echo, ohce
from sample_package.__main__ import main
