'''
sample_package.functions
'''


def echo(string: str) -> None:
    '''Echos the string passed in as s.'''
    print(string)


def ohce(s: str) -> None:
    '''Echos the reverse of the string passed in as s.'''
    print(s[::-1])
